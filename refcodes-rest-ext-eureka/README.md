# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact supports the [Eureka microservice registry/discovery](https://spring.io/guides/gs/service-registration-and-discovery) mechanism (providing a sidecar) to register (attach) your RESTful services to Eureka.***

> See the [`refcodes-rest-ext-eureka`](https://bitbucket.org/refcodes/refcodes-rest-ext/src/master/refcodes-rest-ext-eureka) artifact's example [source code](https://bitbucket.org/refcodes/refcodes-rest-ext/src/master/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka) on how to enable your RESTful services to use Eureka service discovery.

## Getting started ##

> Please refer to the [refcodes-rest: RESTful services using lambdas](https://www.metacodes.pro/refcodes/refcodes-rest) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-rest-ext</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-rest-ext). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-rest-ext).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
