// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Simple launcher for the Eureka server.
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaServer
// @Configuration
public class EurekaServer {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConfigurableApplicationContext _ctx;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public EurekaServer() {}

	public EurekaServer( String[] args ) {
		start( args );
	}

	public EurekaServer( int aPort ) {
		// System.setProperty( "server.port", "" + aPort );
		// System.setProperty( "eureka.client.serviceUrl.defaultZone",
		// "http://localhost:" + aPort + "/eureka" );
		final String[] args = new String[] { "--server.port=" + aPort, "--eureka.client.serviceUrl.defaultZone=http://localhost:" + aPort + "/eureka" };
		start( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// MAIN:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String[] args ) {
		new EurekaServer( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public void start( String[] args ) {
		_ctx = SpringApplication.run( EurekaServer.class, args );
	}

	public void destroy() {
		// WTF, cannot restart any Spring Boot application after shutdown |-->
		SpringApplication.exit( _ctx );
		// WTF, cannot restart any Spring Boot application after shutdown <--|
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	//	@Bean("scheduledThreadPool")
	//	public ExecutorService scheduledThreadPool() {
	//		return Executors.newScheduledThreadPool( 25 );
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////

}
