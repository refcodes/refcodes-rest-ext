// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Scheme;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.JsonMediaTypeFactory;
import org.refcodes.web.MediaTypeFactory;
import org.refcodes.web.Url;

public class EurekaServerDescriptorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEurekaServerDescriptor() throws MarshalException, UnmarshalException {
		final MediaTypeFactory theFactory = new JsonMediaTypeFactory();
		final AmazonMetaData theMetaData = new AmazonMetaData();
		theMetaData.setAmiId( "AmiId" );
		theMetaData.setAmiLaunchIndex( 0 );
		theMetaData.setAmiManifestPath( "AmiManifestPath" );
		theMetaData.setAvailabilityZone( "AvailabilityZone" );
		theMetaData.setHostname( "Hostname" );
		theMetaData.setInstanceId( "InstanceId" );
		theMetaData.setInstanceType( "InstanceType" );
		theMetaData.setLocalHostName( "LocalHostName" );
		theMetaData.setLocalIpv4( "LocalIpv4" );
		theMetaData.setPublicHostname( "PublicHostName" );
		theMetaData.setPublicIpv4( "PublicIpv4" );
		final EurekaServerDescriptor theDescriptor = new EurekaServerDescriptor();
		theDescriptor.setAlias( "Alias" );
		theDescriptor.setAmazonMetaData( theMetaData );
		theDescriptor.setEurekaDataCenterType( EurekaDataCenterType.AMAZON );
		theDescriptor.setEurekaServiceStatus( EurekaServiceStatus.UNKNOWN );
		theDescriptor.setHost( "Host" );
		theDescriptor.setLeaseEvictionDurationInSecs( 120 );
		theDescriptor.setPingUrl( new Url( Scheme.HTTP, "Host", "/ping" ) );
		theDescriptor.setPort( 80 );
		theDescriptor.setVirtualHost( "VirtualHost" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theDescriptor.keySet() ) {
				System.out.println( eKey + ":= " + theDescriptor.get( eKey ) );
			}
		}
		final String theMarshaled = theFactory.toMarshaled( theDescriptor );
		final HttpBodyMap theBodyMap = theFactory.toUnmarshaled( theMarshaled, HttpBodyMap.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theBodyMap.keySet() ) {
				System.out.println( eKey + ":= " + theBodyMap.get( eKey ) );
			}
		}
		assertEquals( theDescriptor.size(), theBodyMap.size() );
		for ( String eKey : theDescriptor.keySet() ) {
			assertEquals( theDescriptor.get( eKey ), theBodyMap.get( eKey ) );
		}
	}
}
