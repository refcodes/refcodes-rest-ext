// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ComponentException;
import org.refcodes.data.Scheme;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.Url;

public class EurekaDiscoverySidecarTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String FOO_BAR = "foobar";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// @Disabled
	@SuppressWarnings("unused")
	@Test
	public void testDiscoverServer() throws HttpStatusException, IOException, ComponentException {
		final Integer theEurekaPort = PortManagerSingleton.getInstance().bindAnyPort();
		final EurekaServer theEurekaServer = new EurekaServer( theEurekaPort );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Starting Eureka on port <" + theEurekaPort + ">: HTTP:\\localhost:" + theEurekaPort );
		}
		// ---------------------------------------------------------------------
		final Integer theServicePort1 = PortManagerSingleton.getInstance().bindAnyPort();
		final Integer theServicePort2 = PortManagerSingleton.getInstance().bindAnyPort();
		final Url theRegistryUrl = new Url( Scheme.HTTP, "localhost", theEurekaPort );
		// ---------------------------------------------------------------------
		final RestfulEurekaServer theServer1 = new EurekaRestServer();
		theServer1.initialize( FOO_BAR, "INSTANCE_A", Scheme.HTTP, null, null, null, theServicePort1, null, theRegistryUrl );
		final RestfulEurekaServer theServer2 = new EurekaRestServer();
		theServer2.initialize( FOO_BAR, "INSTANCE_B", Scheme.HTTP, null, null, null, theServicePort2, null, theRegistryUrl );
		// ---------------------------------------------------------------------
		theServer1.open( theServicePort1 );
		theServer1.start();
		theServer2.open( theServicePort2 );
		theServer2.start();
		// ---------------------------------------------------------------------
		final EurekaDiscoverySidecar theDiscoverySidecar = new EurekaDiscoverySidecar();
		theDiscoverySidecar.initialize( theServer1.getHttpRegistryUrl() );
		theDiscoverySidecar.start();
		final Url theResolved = theDiscoverySidecar.toUrl( Scheme.HTTP, FOO_BAR, "/ping" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResolved.toString() );
		}
		// ---------------------------------------------------------------------
		theServer1.destroy();
		theServer2.destroy();
		theDiscoverySidecar.destroy();
		// ---------------------------------------------------------------------
		// WTF, cannot restart any Spring Boot application after shutdown |-->
		// theEurekaServer.destroy();
		// WTF, cannot restart any Spring Boot application after shutdown <--|
		PortManagerSingleton.getInstance().unbindPort( theServicePort1 );
		PortManagerSingleton.getInstance().unbindPort( theServicePort2 );
		PortManagerSingleton.getInstance().unbindPort( theEurekaPort );
	}
}
