// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ComponentException;
import org.refcodes.data.Delimiter;
import org.refcodes.data.Scheme;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.Url;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

public class EurekaRestServerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// @Disabled
	@SuppressWarnings("unused")
	@Test
	public void testRegisterServer() throws HttpStatusException, IOException, ComponentException {
		final Integer theEurekaPort = PortManagerSingleton.getInstance().bindAnyPort();
		final EurekaServer theEurekaServer = new EurekaServer( theEurekaPort );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Starting Eureka on port <" + theEurekaPort + ">: HTTP:\\localhost:" + theEurekaPort );
		}
		// ---------------------------------------------------------------------
		final Integer theServicePort = PortManagerSingleton.getInstance().bindAnyPort();
		final Url theRegistryUrl = new Url( Scheme.HTTP, "localhost", theEurekaPort );
		RestResponse theResponse;
		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String theBody;
		// ---------------------------------------------------------------------
		final RestfulEurekaServer theServer = new EurekaRestServer();
		theServer.initialize( "foobar", null, Scheme.HTTP, null, null, null, theServicePort, null, theRegistryUrl );
		final RestfulHttpClient theClient = new HttpRestClient();
		theClient.setBaseUrl( theServer.getHttpRegistryUrl() );
		theClient.open();
		theResponse = theClient.doGet( theServer.getAlias() + Delimiter.PATH.getChar() + theServer.getInstanceId() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( theResponse.toDump() ).toString() );
		}
		theBody = gson.toJson( JsonParser.parseString( theResponse.getHttpBody() ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		assertEquals( HttpStatusCode.OK, theResponse.getHttpStatusCode() );
		theServer.open( theServicePort );
		theServer.start();
		theResponse = theClient.doGet( theServer.getAlias() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( theResponse.toDump() ).toString() );
		}
		theBody = gson.toJson( JsonParser.parseString( theResponse.getHttpBody() ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		theResponse = theClient.doGet( theServer.getAlias() + Delimiter.PATH.getChar() + theServer.getInstanceId() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new VerboseTextBuilder().withElements( theResponse.toDump() ).toString() );
		}
		theBody = gson.toJson( JsonParser.parseString( theResponse.getHttpBody() ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBody );
		}
		assertEquals( HttpStatusCode.OK, theResponse.getHttpStatusCode() );
		// ---------------------------------------------------------------------
		theServer.pause();
		theServer.resume();
		theServer.stop();
		theServer.destroy();
		// ---------------------------------------------------------------------
		// WTF, cannot restart any Spring Boot application after shutdown |-->
		// theEurekaServer.destroy();
		// WTF, cannot restart any Spring Boot application after shutdown <--|
		PortManagerSingleton.getInstance().unbindPort( theServicePort );
		PortManagerSingleton.getInstance().unbindPort( theEurekaPort );
	}
}
