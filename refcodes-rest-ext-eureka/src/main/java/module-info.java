module org.refcodes.rest.ext.eureka {
	requires software.amazon.awssdk.core;
	requires software.amazon.awssdk.utils;
	requires software.amazon.awssdk.regions;
	requires org.refcodes.runtime;
	requires org.refcodes.struct;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.web;
	requires transitive org.refcodes.net;
	requires transitive org.refcodes.rest;
	requires transitive org.refcodes.security;
	requires java.logging;

	exports org.refcodes.rest.ext.eureka;

	opens org.refcodes.rest.ext.eureka; // Opens access for unnamed modules (our spring boot test)
}
