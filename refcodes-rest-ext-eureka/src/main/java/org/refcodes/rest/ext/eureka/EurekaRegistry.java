// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.component.InitializeException;
import org.refcodes.data.Scheme;
import org.refcodes.rest.HomePathAccessor.HomePathBuilder;
import org.refcodes.rest.HomePathAccessor.HomePathProperty;
import org.refcodes.rest.HttpRegistry;
import org.refcodes.rest.RestfulHttpRegistryServer;
import org.refcodes.rest.StatusPathAccessor.StatusPathBuilder;
import org.refcodes.rest.StatusPathAccessor.StatusPathProperty;
import org.refcodes.rest.ext.eureka.EurekaDataCenterTypeAccessor.EurekaDataCenterTypeBuilder;
import org.refcodes.rest.ext.eureka.EurekaDataCenterTypeAccessor.EurekaDataCenterTypeProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * The {@link EurekaRegistry} describes the functionality required in order to
 * register a service at a service registry and discovery service. This type is
 * intended to be used by different separate hierarchy branches by providing the
 * generic type &lt;B&gt;, ensuring a coherent type hierarchy for each branch.
 *
 * @param <B> In order to implement the builder pattern with a coherent type
 *        hierarchy.
 */
public interface EurekaRegistry<B extends EurekaRegistry<B>> extends HttpRegistry<EurekaServerDescriptor, B>, EurekaDataCenterTypeProperty, EurekaDataCenterTypeBuilder<B>, HomePathProperty, HomePathBuilder<B>, StatusPathProperty, StatusPathBuilder<B>, EurekaServerDescriptorFactory {

	String EUREKA_BASE_PATH = "/eureka/apps";

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withEurekaDataCenterType( EurekaDataCenterType aDataCenterType ) {
		setEurekaDataCenterType( aDataCenterType );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withHomePath( String aHomePath ) {
		setHomePath( aHomePath );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	default B withStatusPath( String aStatusPath ) {
		setStatusPath( aStatusPath );
		return (B) this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, Url aRegistryUrl ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, null, null, null ), aRegistryUrl, null );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry.
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aRegistryUrl The registry server where to register.
	 * @param aDataCenterType The type of the data center your
	 *        {@link RestfulEurekaServer} is running in.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, EurekaDataCenterType aDataCenterType, Url aRegistryUrl ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, null, null, aDataCenterType ), aRegistryUrl );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry.
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aStatusPath The path to use as status-page end-point by this
	 * @param aHomePath The path to use as home-page end-point by this
	 * @param aDataCenterType The type of the data center your
	 *        {@link RestfulEurekaServer} is running in.
	 * @param aRegistryUrl The registry server where to register.
	 * @param aStoreDescriptor The descriptor describing the truststore for
	 *        (optionally) opening an HTTPS connection to the registry server.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, String aStatusPath, String aHomePath, EurekaDataCenterType aDataCenterType, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, aStatusPath, aHomePath, aDataCenterType ), aRegistryUrl, aStoreDescriptor );
	}
}
