// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.Destroyable;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.Startable;
import org.refcodes.component.StopException;
import org.refcodes.component.Stoppable;
import org.refcodes.exception.Trap;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.rest.AbstractHttpDiscoverySidecar;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.Url;
import org.refcodes.web.UrlBuilder;

/**
 * The {@link EurekaDiscoverySidecar} decorates a {@link RestfulHttpClient} with
 * functionality such registering and unregistering from / to a Eureka discovery
 * service.
 */
public class EurekaDiscoverySidecar extends AbstractHttpDiscoverySidecar<EurekaDiscoverySidecar> implements EurekaDiscovery<EurekaDiscoverySidecar> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( EurekaDiscoverySidecar.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutorService _executorService;
	private RefreshDaemon _refreshDaemon;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link EurekaDiscoverySidecar} with discovery functionality.
	 */
	public EurekaDiscoverySidecar() {
		this( null );
	}

	/**
	 * Constructs a {@link EurekaDiscoverySidecar} with discovery functionality.
	 * 
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public EurekaDiscoverySidecar( ExecutorService aExecutorService ) {
		_executorService = aExecutorService;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFE-CYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize( Url aDiscoveryUrl, LoadBalancingStrategy aStrategy, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		aDiscoveryUrl = toHttpDiscoveryUrl( aDiscoveryUrl, this );
		aStrategy = toLoadBalancingStrategy( aStrategy, this );
		super.initialize();
		setLoadBalancingStrategy( aStrategy );
		try {
			_refreshDaemon = new RefreshDaemon( aDiscoveryUrl, aStoreDescriptor, this, _executorService );
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new InitializeException( "Cannot enter the initialize lifecycle!", e );
		}
	}

	@Override
	public synchronized void start() throws StartException {
		try {
			super.start();
			_refreshDaemon.start();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StartException( "Cannot enter the start lifecycle!", e );
		}
	}

	@Override
	public synchronized void pause() throws PauseException {
		super.pause();
		try {}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new PauseException( "Cannot enter the pause lifecycle!", e );
		}
	}

	@Override
	public synchronized void stop() throws StopException {
		super.stop();
		try {
			_refreshDaemon.stop();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StopException( "Cannot enter the stop lifecycle!", e );
		}
	}

	@Override
	public synchronized void resume() throws ResumeException {
		super.resume();
		try {}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new ResumeException( "Cannot enter the resume lifecycle!", e );
		}
	}

	@Override
	public synchronized void destroy() {
		super.destroy();
		try {
			_refreshDaemon.destroy();
		}
		catch ( Exception e ) {
			LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );

		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url toUrl( Url aUrl ) {
		return toUrl( aUrl, this, _refreshDaemon );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected static Url toUrl( Url aUrl, EurekaDiscovery<?> aDiscovery, RefreshDaemon aRefreshDaemon ) {
		String theHost = aUrl.getHost();
		if ( theHost != null ) {
			theHost = theHost.toUpperCase();
		}
		final List<EurekaInstanceDescriptor> theInstances = aRefreshDaemon.getInstance( theHost );
		if ( theInstances != null ) {
			final EurekaInstanceDescriptor[] theArray = theInstances.toArray( new EurekaInstanceDescriptor[theInstances.size()] );
			if ( theArray.length > 0 ) {
				switch ( aDiscovery.getLoadBalancingStrategy() ) {
				case CUSTOM:
					throw new IllegalStateException( "The load balancing satrategy <" + LoadBalancingStrategy.CUSTOM + "> is cunnrently not supported." );
				case NONE:
					break;
				case RANDOM:
					final int theRnd = ThreadLocalRandom.current().nextInt( 0, theArray.length );
					final UrlBuilder theUrl = new UrlBuilder( aUrl );
					final int[] theIAddr = theArray[theRnd].getIpAddress();
					// Prefer IP over host, host may abused as instance TID |-->
					if ( theIAddr != null ) {
						theUrl.setIpAddress( theIAddr );
					}
					else {
						theUrl.setHost( theArray[theRnd].getHost() );
					}
					// Prefer IP over host, host may abused as instance TID <--|
					theUrl.setPort( theArray[theRnd].getPort() );
					return theUrl;
				case RANDOM_STICKY:
					throw new IllegalStateException( "The load balancing satrategy <" + LoadBalancingStrategy.RANDOM_STICKY + "> is cunnrently not supported." );
				case ROUND_ROBIN:
					throw new IllegalStateException( "The load balancing satrategy <" + LoadBalancingStrategy.ROUND_ROBIN + "> is cunnrently not supported." );
				default:
					throw new UnhandledEnumBugException( aDiscovery.getLoadBalancingStrategy() );
				}
			}
		}
		return aUrl;
	}

	/**
	 * Resolves the property from the provided value and the provided property
	 * and sets the property in case the provided value is not null.
	 * 
	 * @param aDiscoveryUrl The value to be used when not null.
	 * @param aProperty The property to be used when the value is null and which
	 *        is to be set when the value is not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected static Url toHttpDiscoveryUrl( Url aDiscoveryUrl, HttpDiscoveryUrlProperty aProperty ) {
		if ( aDiscoveryUrl != null ) {
			aProperty.setHttpDiscoveryUrl( aDiscoveryUrl );
		}
		else {
			aDiscoveryUrl = aProperty.getHttpDiscoveryUrl();
		}
		if ( aDiscoveryUrl != null && aDiscoveryUrl.getPath() == null ) {
			aDiscoveryUrl = new Url( aDiscoveryUrl, RestfulEurekaServer.EUREKA_BASE_PATH );
			aProperty.setHttpDiscoveryUrl( aDiscoveryUrl );
		}
		return aDiscoveryUrl;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Attention: This class is package local! As it does some well known casts
	 * which are not obvious from the constructor's signature! !!! ONLY INTENDED
	 * TO BE USED BY THE MAINTAINER OF THIS CLASS !!!
	 */
	static class RefreshDaemon extends TimerTask implements Startable, Stoppable, Destroyable {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private Map<String, List<EurekaInstanceDescriptor>> _instances;
		private EurekaDiscovery<?> _discovery;
		private ExecutorService _executorService;
		private Timer _scheduler;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTIRS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Attention: This constructor is package local! As it does some well
		 * known casts which are not obvious from the constructor's signature!
		 * !!! ONLY INTENDED TO BE USED BY THE MAINTAINER OF THIS CLASS !!!
		 */
		RefreshDaemon( Url aDiscoveryUrl, TrustStoreDescriptor aStoreDescriptor, EurekaDiscovery<?> aDiscovery, ExecutorService aExecutorService ) throws HttpStatusException, IOException {
			_executorService = aExecutorService;
			_discovery = aDiscovery;
			_instances = loadRegisteredServices( aDiscoveryUrl, aStoreDescriptor );
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Returns all instances retrieved from the targeted Eureka service.
		 * 
		 * @return The instances as retrieved from the Eureka service.
		 */
		public Map<String, List<EurekaInstanceDescriptor>> getInstances() {
			return _instances;
		}

		/**
		 * Returns the instance represented by the given alias or null if none
		 * such instance was found.
		 * 
		 * @param aAlias The alias for which to resolve the instance.
		 * 
		 * @return The according instance or null if none such instance was
		 *         found for the given alias.
		 */
		public List<EurekaInstanceDescriptor> getInstance( String aAlias ) {
			return _instances.get( aAlias );
		}

		// /////////////////////////////////////////////////////////////////////
		// SCHEDULER:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void start() {
			_scheduler = new Timer( true );
			_scheduler.schedule( this, EurekaLoopSleepTime.DISCOVERY_SERVICE_REFRESH.getTimeMillis(), EurekaLoopSleepTime.DISCOVERY_SERVICE_REFRESH.getTimeMillis() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void stop() {
			_scheduler.cancel();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void destroy() {
			_scheduler.cancel();
			_discovery = null;
			_executorService = null;
			_instances.clear();
			_instances = null;
		}

		// /////////////////////////////////////////////////////////////////////
		// DAEMON:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void run() {
			if ( _discovery.isRunning() ) {
				LOGGER.info( "Refreshing clients from <" + _discovery.getHttpDiscoveryUrl().toHttpUrl() + "> ..." );
				try {
					final Map<String, List<EurekaInstanceDescriptor>> theNewInstances = loadRegisteredServices( _discovery.getHttpDiscoveryUrl(), ( (TrustStoreDescriptorProperty) _discovery ).getTrustStoreDescriptor() );
					List<EurekaInstanceDescriptor> eInstances;
					for ( String eInstanceId : theNewInstances.keySet() ) {
						synchronized ( this ) {
							eInstances = toInstancesUpdate( theNewInstances.get( eInstanceId ), _instances.get( eInstanceId ) );
							_instances.put( eInstanceId, eInstances );
						}
					}

				}
				catch ( HttpStatusException | IOException e ) {
					LOGGER.log( Level.WARNING, "Cannot refresh clients from discovery registry <" + _discovery.getHttpDiscoveryUrl().toHttpUrl() + "> as of : " + Trap.asMessage( e ), e );
				}

			}
		}

		// /////////////////////////////////////////////////////////////////////
		// HELPER:
		// /////////////////////////////////////////////////////////////////////

		private Map<String, List<EurekaInstanceDescriptor>> loadRegisteredServices( Url aDiscoveryUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException, HttpStatusException {
			final Map<String, List<EurekaInstanceDescriptor>> theInstances = new HashMap<>();
			aDiscoveryUrl = toHttpDiscoveryUrl( aDiscoveryUrl, _discovery );
			aStoreDescriptor = toTrustStoreDescriptor( aStoreDescriptor, (TrustStoreDescriptorProperty) _discovery );
			final RestfulHttpClient theClient = new HttpRestClient( _executorService );
			theClient.open( aStoreDescriptor );
			theClient.setBaseUrl( aDiscoveryUrl );
			LOGGER.info( "Requesting discoverable services at <" + aDiscoveryUrl.toHttpUrl() + "> Eureka service registry..." );
			final RestResponse theResponse = theClient.doGet( "/" );
			if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
				throw theResponse.getHttpStatusCode().toHttpStatusException( "Cannot retrieve any discoverable services with service discovery <" + aDiscoveryUrl.toHttpUrl() + "> due to HTTP-Status-Code " + theResponse.getHttpStatusCode() + "<" + theResponse.getHttpStatusCode().getStatusCode() + ">: " + theResponse.getHttpBody() );
			}
			final HttpBodyMap theHttpBody = theResponse.getResponse();
			final HttpBodyMap theCloud = theHttpBody.retrieveFrom( theHttpBody.toPath( "applications", "application" ) );
			final Set<String> theAppsListing = theCloud.dirs();
			Set<String> eInstancesListing;
			HttpBodyMap eApp;
			List<EurekaInstanceDescriptor> eDescriptors;
			EurekaInstanceDescriptor eDescriptor;
			String eAlias;
			for ( String eAppIndex : theAppsListing ) {
				eApp = theCloud.retrieveFrom( theCloud.toPath( eAppIndex, "instance" ) );
				eInstancesListing = eApp.dirs();
				for ( String eInstanceIndex : eInstancesListing ) {
					eDescriptor = new EurekaInstanceDescriptor( eApp.retrieveFrom( eInstanceIndex ) );
					eAlias = eDescriptor.getAlias();
					if ( eAlias != null ) {
						eAlias = eAlias.toUpperCase();
					}
					eDescriptors = theInstances.get( eAlias );
					if ( eDescriptors == null ) {
						synchronized ( _discovery ) {
							eDescriptors = theInstances.get( eAlias );
							if ( eDescriptors == null ) {
								eDescriptors = new ArrayList<>();
								theInstances.put( eAlias, eDescriptors );
							}
						}
					}
					eDescriptors.add( eDescriptor );
				}
			}
			return theInstances;
		}

		private List<EurekaInstanceDescriptor> toInstancesUpdate( List<EurekaInstanceDescriptor> aRemoteInstances, List<EurekaInstanceDescriptor> aLocalInstances ) {
			final List<EurekaInstanceDescriptor> theInstances = new ArrayList<>( aLocalInstances );
			EurekaInstanceDescriptor eLocalInstance;
			if ( aRemoteInstances != null ) {
				for ( EurekaInstanceDescriptor eRemoteInstance : aRemoteInstances ) {
					eLocalInstance = toInstance( eRemoteInstance, theInstances );
					if ( eLocalInstance != null ) {
						theInstances.remove( eLocalInstance );
					}
					if ( EurekaServiceStatus.UP == EurekaServiceStatus.toStatus( eRemoteInstance.getStatus() ) ) {
						theInstances.add( eRemoteInstance );
					}
				}
			}
			return theInstances;
		}

		private EurekaInstanceDescriptor toInstance( EurekaInstanceDescriptor aInstance, List<EurekaInstanceDescriptor> aInstances ) {
			if ( aInstances != null ) {
				for ( EurekaInstanceDescriptor eInstance : aInstances ) {
					if ( eInstance.getHost().equals( aInstance.getHost() ) ) {
						return eInstance;
					}
				}
			}
			return null;
		}
	}
}
