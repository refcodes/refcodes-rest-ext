// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.mixin.NameAccessor;

/**
 * The data center types known by Eureka.
 */
public enum EurekaDataCenterType implements NameAccessor {

	/**
	 * You are running on a Non-Amazon data-center.
	 */
	MY_OWN("MyOwn"),

	/**
	 * You are running on an Amazon data-center.
	 */
	AMAZON("Amazon");

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private EurekaDataCenterType( String aName ) {
		_name = aName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getName();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Resolves an {@link EurekaDataCenterType} from the given name case
	 * insensitive.
	 * 
	 * @param aName The name for which to resolve case insensitive the
	 *        {@link EurekaDataCenterType}.
	 * 
	 * @return The according {@link EurekaDataCenterType} or null if the name
	 *         was not resolvable.
	 */
	public static EurekaDataCenterType fromName( String aName ) {
		for ( EurekaDataCenterType eType : values() ) {
			if ( eType.getName().equalsIgnoreCase( aName ) ) {
				return eType;
			}
		}
		return null;
	}
}
