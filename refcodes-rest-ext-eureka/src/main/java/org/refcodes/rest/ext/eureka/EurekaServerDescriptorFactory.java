// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.data.Scheme;
import org.refcodes.rest.HttpRegistrySidecar;
import org.refcodes.rest.HttpServerDescriptor;
import org.refcodes.rest.HttpServerDescriptorFactory;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * Implementation of the {@link EurekaServerDescriptorFactory}.
 */
public interface EurekaServerDescriptorFactory extends HttpServerDescriptorFactory<EurekaServerDescriptor> {

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify theinstance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * 
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing this instance via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	default EurekaServerDescriptor toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, EurekaDataCenterType aDataCenterType ) {
		return toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, null, null, aDataCenterType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default EurekaServerDescriptor toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath ) {
		return toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, null );
	}

	/**
	 * Prepares the {@link HttpServerDescriptor} by creating it from this
	 * instance's state and the provided arguments. The provided arguments can
	 * modify theinstance's state. The {@link HttpServerDescriptor} as finally
	 * used is returned. You may modify this context and use it after
	 * modification to initialize the server via
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url)} or
	 * {@link HttpRegistrySidecar#initialize(HttpServerDescriptor, Url, TrustStoreDescriptor)}.
	 * {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aStatusPath The path to use as status-page end-point by this
	 *        server.
	 * @param aHomePath The path to use as home-page end-point by this server.
	 * @param aDataCenterType The data center type to be used.
	 * 
	 * @return The {@link HttpServerDescriptor} as would be used when
	 *         initializing this instance via
	 *         {@link HttpRegistrySidecar#initialize()}
	 */
	EurekaServerDescriptor toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, String aStatusPath, String aHomePath, EurekaDataCenterType aDataCenterType );

}