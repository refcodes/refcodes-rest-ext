// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.net.MalformedURLException;
import java.util.Map;

import org.refcodes.exception.BugException;
import org.refcodes.mixin.AliasAccessor.AliasProperty;
import org.refcodes.mixin.PortAccessor.PortProperty;
import org.refcodes.net.IpAddress;
import org.refcodes.net.IpAddressAccessor.IpAddressProperty;
import org.refcodes.rest.HomeUrlAccessor.HomeUrlProperty;
import org.refcodes.rest.HttpServerDescriptor;
import org.refcodes.rest.PingUrlAccessor.PingUrlProperty;
import org.refcodes.rest.StatusUrlAccessor.StatusUrlProperty;
import org.refcodes.rest.ext.eureka.AmazonMetaDataAccessor.AmazonMetaDataProperty;
import org.refcodes.rest.ext.eureka.EurekaDataCenterTypeAccessor.EurekaDataCenterTypeProperty;
import org.refcodes.rest.ext.eureka.EurekaServiceStatusAccessor.EurekaServiceStatusProperty;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.web.HostAccessor.HostProperty;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.Url;

/**
 * The {@link EurekaServerDescriptor} refines the {@link HttpServerDescriptor}
 * for use with an Eureka Service-Registry.
 */
public class EurekaServerDescriptor extends HttpBodyMap implements HttpServerDescriptor, AliasProperty, PingUrlProperty, StatusUrlProperty, HomeUrlProperty, HostProperty, IpAddressProperty, PortProperty, EurekaDataCenterTypeProperty, EurekaServiceStatusProperty, AmazonMetaDataProperty {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the length of lease - public if 90 seconds.
	 * 
	 * @return The lease eviction duration in seconds.
	 */
	public Integer getLeaseEvictionDurationInSecs() {
		return getInt( "instance/leaseInfo/evictionDurationInSecs" );
	}

	/**
	 * Optional, if you want to change the length of lease - public if 90
	 * seconds.
	 * 
	 * @param aLeaseEvictionDurationInSecs The lease eviction duration in
	 *        seconds.
	 */
	public void setLeaseEvictionDurationInSecs( Integer aLeaseEvictionDurationInSecs ) {
		putInt( "instance/leaseInfo/evictionDurationInSecs", aLeaseEvictionDurationInSecs );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return get( "instance/app" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAlias( String aAlias ) {
		put( "instance/app", aAlias );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getPingUrl() {
		final String theHealthCheckUrl = get( "instance/healthCheckUrl" );
		if ( theHealthCheckUrl == null ) {
			return null;
		}
		try {
			return new Url( theHealthCheckUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theHealthCheckUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPingUrl( Url aUrl ) {
		put( "instance/healthCheckUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getStatusUrl() {
		final String theStatusPageUrl = get( "instance/statusPageUrl" );
		if ( theStatusPageUrl == null ) {
			return null;
		}
		try {
			return new Url( theStatusPageUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theStatusPageUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStatusUrl( Url aUrl ) {
		put( "instance/statusPageUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHomeUrl() {
		final String theHomePageUrl = get( "instance/homePageUrl" );
		if ( theHomePageUrl == null ) {
			return null;
		}
		try {
			return new Url( theHomePageUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theHomePageUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHomeUrl( Url aUrl ) {
		put( "instance/homePageUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHost() {
		return get( "instance/hostName" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHost( String aHost ) {
		put( "instance/hostName", aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPort() {
		return getInt( "instance/port" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPort( int aPort ) {
		putInt( "instance/port", aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getIpAddress() {
		return IpAddress.fromAnyCidrNotation( get( "instance/ipAddr" ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIpAddress( int[] aIpAddress ) {
		put( "instance/ipAddr", IpAddress.toString( aIpAddress ) );
	}

	/**
	 * Retrieves the virtual host as of Eureka(?).
	 * 
	 * @return The virtual host.
	 */
	public String getVirtualHost() {
		return get( "instance/vipAddress" );
	}

	/**
	 * Sets the virtual host as of Eureka(?).
	 * 
	 * @param aVirtualHost The virtual host.
	 */
	public void setVirtualHost( String aVirtualHost ) {
		put( "instance/vipAddress", aVirtualHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaDataCenterType getEurekaDataCenterType() {
		return EurekaDataCenterType.fromName( get( "instance/dataCenterInfo/name" ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEurekaDataCenterType( EurekaDataCenterType aDataCenterType ) {
		put( "instance/dataCenterInfo/name", aDataCenterType.getName() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AmazonMetaData getAmazonMetaData() {
		return toType( "instance/dataCenterInfo/metadata", AmazonMetaData.class );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAmazonMetaData( AmazonMetaData aDataCenterType ) {
		insertTo( "instance/dataCenterInfo/metadata", (CanonicalMap) aDataCenterType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaServiceStatus getEurekaServiceStatus() {
		return EurekaServiceStatus.valueOf( get( "instance/status" ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEurekaServiceStatus( EurekaServiceStatus aServiceStatus ) {
		put( "instance/status", aServiceStatus.name() );
	}

	/**
	 * Retrieves the application's Meta-Data.
	 * 
	 * @return The {@link Map} representing the applications's Meta-Data.
	 */
	public Map<String, String> getMetaData() {
		return retrieveFrom( "instance/metadata" );
	}

	/**
	 * Sets the application's Meta-Data.
	 * 
	 * @param aMetaData The {@link Map} representing the applications's
	 *        Meta-Data.
	 */
	public void setMetaData( Map<String, String> aMetaData ) {
		for ( String eKey : aMetaData.keySet() ) {
			put( toPath( "instance/metadata", eKey ), aMetaData.get( eKey ) );
		}
	}

	/**
	 * Adds a key-value pair to the application's Meta-Data.
	 * 
	 * @param aKey The Key of the Meta-Data entry.
	 * @param aValue The value for the Meta-Data entry.
	 * 
	 * @return The previously set value for the given Meta-Data entry or null if
	 *         none was set before.
	 */
	public String putMetaData( String aKey, String aValue ) {
		return put( toPath( "instance/metadata", aKey ), aValue );
	}
}
