// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

/**
 * Provides an accessor for a Eureka data center type property.
 */
public interface EurekaDataCenterTypeAccessor {

	/**
	 * Retrieves the Eureka data center type from the Eureka data center type
	 * property.
	 * 
	 * @return The Eureka data center type stored by the Eureka data center type
	 *         property.
	 */
	EurekaDataCenterType getEurekaDataCenterType();

	/**
	 * Provides a mutator for a Eureka data center type property.
	 */
	public interface EurekaDataCenterTypeMutator {

		/**
		 * Sets the Eureka data center type for the Eureka data center type
		 * property.
		 * 
		 * @param aDataCenterType The Eureka data center type to be stored by
		 *        the Eureka data center type property.
		 */
		void setEurekaDataCenterType( EurekaDataCenterType aDataCenterType );
	}

	/**
	 * Provides a mutator for an Eureka data center type property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link EurekaDataCenterTypeBuilder}.
	 */
	public interface EurekaDataCenterTypeBuilder<B extends EurekaDataCenterTypeBuilder<?>> {

		/**
		 * Sets the Eureka data center type to use and returns this builder as
		 * of the builder pattern.
		 * 
		 * @param aDataCenterType The Eureka data center type to be stored by
		 *        the Eureka data center type property.
		 * 
		 * @return This {@link EurekaDataCenterTypeBuilder} instance to continue
		 *         configuration.
		 */
		B withEurekaDataCenterType( EurekaDataCenterType aDataCenterType );
	}

	/**
	 * Provides a Eureka data center type property.
	 */
	public interface EurekaDataCenterTypeProperty extends EurekaDataCenterTypeAccessor, EurekaDataCenterTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link EurekaDataCenterType} (setter) as of
		 * {@link #setEurekaDataCenterType(EurekaDataCenterType)} and returns
		 * the very same value (getter).
		 * 
		 * @param aEurekaDataCenterType The {@link EurekaDataCenterType} to set
		 *        (via {@link #setEurekaDataCenterType(EurekaDataCenterType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default EurekaDataCenterType letEurekaDataCenterType( EurekaDataCenterType aEurekaDataCenterType ) {
			setEurekaDataCenterType( aEurekaDataCenterType );
			return aEurekaDataCenterType;
		}
	}
}
