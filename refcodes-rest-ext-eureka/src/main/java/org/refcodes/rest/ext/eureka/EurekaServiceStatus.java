// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

/**
 * The {@link EurekaServiceStatus} enumerates the known Eureka service states as
 * of "https://github.com/Netflix/eureka/wiki/Eureka-REST-operations".
 */
public enum EurekaServiceStatus {
	STARTING, UP, DOWN, OUT_OF_SERVICE, UNKNOWN;

	/**
	 * Retrieves the {@link EurekaServiceStatus} from the provided status' name,
	 * ignoring the case.
	 * 
	 * @param aStatusName The name for which to resolve the
	 *        {@link EurekaServiceStatus}.
	 * 
	 * @return The resolved status or null if the name was not resolvable.
	 */
	public static EurekaServiceStatus toStatus( String aStatusName ) {
		if ( aStatusName != null && aStatusName.length() != 0 ) {
			for ( EurekaServiceStatus eStatus : values() ) {
				if ( eStatus.name().equalsIgnoreCase( aStatusName ) ) {
					return eStatus;
				}
			}
		}
		return null;
	}
}
