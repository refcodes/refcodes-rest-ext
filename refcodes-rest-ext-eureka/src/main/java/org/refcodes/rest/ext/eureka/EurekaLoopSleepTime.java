// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.mixin.TimeMillisAccessor;

/**
 * Enumeration with common timeouts regarding Eureka service registry and
 * discovery.
 */
public enum EurekaLoopSleepTime implements TimeMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// EUREKA LOOP SLEEP TIMES:
	// /////////////////////////////////////////////////////////////////////////

	REGISTRY_SERVICE_HEARBEAT(30000),

	DISCOVERY_SERVICE_REFRESH(30000);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new daemon loop sleep time.
	 *
	 * @param aValue the value
	 */
	private EurekaLoopSleepTime( int aValue ) {
		_value = aValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTimeMillis() {
		return _value;
	}
}
