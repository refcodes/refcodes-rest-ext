// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact supports the <a href=
 * "https://spring.io/guides/gs/service-registration-and-discovery">Eureka
 * microservice registry/discovery</a> mechanism (providing a sidecar) to
 * register (attach) your RESTful services to Eureka.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-rest"><strong>refcodes-rest:
 * RESTful services using lambdas</strong></a> documentation for an up-to-date
 * and detailed description on the usage of this artifact.
 * </p>
 * 
 * See the this artifact's example <a href=
 * "https://bitbucket.org/refcodes/refcodes-rest-ext/src/master/refcodes-rest-ext-eureka/src/test/java/org/refcodes/rest/ext/eureka">source
 * code</a> on how to enable your RESTful services to use Eureka service
 * discovery.
 */
package org.refcodes.rest.ext.eureka;
