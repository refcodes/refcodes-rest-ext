// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

/**
 * Provides an accessor for a Eureka service status property.
 */
public interface EurekaServiceStatusAccessor {

	/**
	 * Retrieves the Eureka service status from the Eureka service status
	 * property.
	 * 
	 * @return The Eureka service status stored by the Eureka service status
	 *         property.
	 */
	EurekaServiceStatus getEurekaServiceStatus();

	/**
	 * Provides a mutator for a Eureka service status property.
	 */
	public interface EurekaServiceStatusMutator {

		/**
		 * Sets the Eureka service status for the Eureka service status
		 * property.
		 * 
		 * @param aServiceStatus The Eureka service status to be stored by the
		 *        Eureka service status property.
		 */
		void setEurekaServiceStatus( EurekaServiceStatus aServiceStatus );
	}

	/**
	 * Provides a mutator for an Eureka service status property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link EurekaServiceStatusBuilder}.
	 */
	public interface EurekaServiceStatusBuilder<B extends EurekaServiceStatusBuilder<?>> {

		/**
		 * Sets the Eureka service status to use and returns this builder as of
		 * the builder pattern.
		 * 
		 * @param aServiceStatus The Eureka service status to be stored by the
		 *        Eureka service status property.
		 * 
		 * @return This {@link EurekaServiceStatusBuilder} instance to continue
		 *         configuration.
		 */
		B withEurekaServiceStatus( EurekaServiceStatus aServiceStatus );
	}

	/**
	 * Provides a Eureka service status property.
	 */
	public interface EurekaServiceStatusProperty extends EurekaServiceStatusAccessor, EurekaServiceStatusMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link EurekaServiceStatus} (setter) as of
		 * {@link #setEurekaServiceStatus(EurekaServiceStatus)} and returns the
		 * very same value (getter).
		 * 
		 * @param aEurekaServiceStatus The {@link EurekaServiceStatus} to set
		 *        (via {@link #setEurekaServiceStatus(EurekaServiceStatus)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default EurekaServiceStatus letEurekaServiceStatus( EurekaServiceStatus aEurekaServiceStatus ) {
			setEurekaServiceStatus( aEurekaServiceStatus );
			return aEurekaServiceStatus;
		}
	}
}
