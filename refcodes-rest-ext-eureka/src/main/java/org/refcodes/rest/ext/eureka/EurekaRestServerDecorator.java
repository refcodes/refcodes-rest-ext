// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.exception.Trap;
import org.refcodes.rest.AbstractRestfulHttpRegistryServerDecorator;
import org.refcodes.rest.HttpExceptionHandler;
import org.refcodes.rest.HttpExceptionHandling;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import org.refcodes.web.Url;

/**
 * The {@link EurekaRestServerDecorator} implements the
 * {@link RestfulEurekaServer} interface and decorates a given
 * {@link RestfulHttpServer} with functionality such registering and
 * unregistering from or to an Eureka discovery service. Follow the
 * documentation of the {@link RestfulEurekaServer} in order to initiate the
 * states such as {@link EurekaServiceStatus#UP},
 * {@link EurekaServiceStatus#DOWN} or
 * {@link EurekaServiceStatus#OUT_OF_SERVICE} and unregistering.
 */
public class EurekaRestServerDecorator extends AbstractRestfulHttpRegistryServerDecorator<EurekaServerDescriptor, RestfulEurekaServer> implements RestfulEurekaServer {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( EurekaRestServerDecorator.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutorService _executorService;
	private Timer _scheduler;
	private EurekaDataCenterType _dataCenterType;
	private String _statusPath;
	private String _homePath;
	private RestRequestConsumer _homeRequestConsumer;
	private RestRequestConsumer _statusRequestConsumer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpServer} with discovery
	 * functionality. Use {@link #initialize()}, {@link #start()},
	 * {@link #pause()}, {@link #resume()}, {@link #stop()} and
	 * {@link #destroy()} for publishing status updates to Eureka. Use
	 * {@link #open(org.refcodes.web.HttpServerContext)} or similar to activate
	 * your server.
	 * 
	 * @param aServer The {@link RestfulHttpServer} to be decorated.
	 */
	public EurekaRestServerDecorator( RestfulHttpServer aServer ) {
		super( aServer );
	}

	/**
	 * Decorates the given {@link RestfulHttpServer} with discovery
	 * functionality. Use {@link #initialize()}, {@link #start()},
	 * {@link #pause()}, {@link #resume()}, {@link #stop()} and
	 * {@link #destroy()} for publishing status updates to Eureka. Use
	 * {@link #open(org.refcodes.web.HttpServerContext)} or similar to activate
	 * your server.
	 * 
	 * @param aServer The {@link RestfulHttpServer} to be decorated.
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public EurekaRestServerDecorator( RestfulHttpServer aServer, ExecutorService aExecutorService ) {
		super( aServer );
		_executorService = aExecutorService;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void initialize( EurekaServerDescriptor aServerDescriptor, RestRequestConsumer aPingObserver, RestRequestConsumer aStatusObserver, RestRequestConsumer aHomeObserver, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		super.initialize();

		aPingObserver = toPingObserver( aPingObserver );
		aHomeObserver = toHomeObserver( aHomeObserver );
		aStatusObserver = toStatusObserver( aStatusObserver );
		aRegistryUrl = toHttpRegistryUrl( aRegistryUrl );
		aStoreDescriptor = toTrustStoreDescriptor( aStoreDescriptor );
		aServerDescriptor = toHttpServerDescriptor( aServerDescriptor );

		try {
			if ( aPingObserver != null ) {
				if ( aServerDescriptor.getPingUrl() == null ) {
					throw new InitializeException( "Cannot register a PING observer without a PING path being defined in the server descriptor!" );
				}
				onGet( aServerDescriptor.getPingUrl().getPath(), aPingObserver );
			}
			if ( aStatusObserver != null ) {
				if ( aServerDescriptor.getStatusUrl() == null ) {
					throw new InitializeException( "Cannot register a STATUS observer without a STATUS path being defined in the server descriptor!" );
				}
				onGet( aServerDescriptor.getStatusUrl().getPath(), aStatusObserver );
			}
			if ( aHomeObserver != null ) {
				if ( aServerDescriptor.getHomeUrl() == null ) {
					throw new InitializeException( "Cannot register a HOME observer without a HOME path being defined in the server descriptor!" );
				}
				onGet( aServerDescriptor.getHomeUrl().getPath(), aHomeObserver );
			}
			disableObservers();
			doRegister( EurekaServiceStatus.STARTING );
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new InitializeException( "Cannot enter the initialize lifecycle!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void start() throws StartException {
		try {
			if ( !isOpened() ) {
				LOGGER.log( Level.WARNING, "This connection not opened yet (it is in status <" + getConnectionStatus() + ">, therefore will try to open this connection now..." );
				open();
			}
			super.start();
			enableObservers();
			doStatusUpdate( EurekaServiceStatus.UP );
			_scheduler = new Timer( true );
			_scheduler.schedule( new EurekaRegistrySidecar.HeartBeatDaemon( this, _executorService ), EurekaLoopSleepTime.REGISTRY_SERVICE_HEARBEAT.getTimeMillis(), EurekaLoopSleepTime.REGISTRY_SERVICE_HEARBEAT.getTimeMillis() );
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StartException( "Cannot enter the start lifecycle!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void pause() throws PauseException {
		super.pause();
		try {
			doStatusUpdate( EurekaServiceStatus.DOWN );
			disableObservers();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new PauseException( "Cannot enter the pause lifecycle!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void resume() throws ResumeException {
		super.resume();
		try {
			enableObservers();
			doStatusUpdate( EurekaServiceStatus.UP );
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new ResumeException( "Cannot enter the resume lifecycle!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void stop() throws StopException {
		super.stop();
		try {
			_scheduler.cancel();
			doStatusUpdate( EurekaServiceStatus.OUT_OF_SERVICE );
			disableObservers();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StopException( "Cannot enter the stop lifecycle!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void destroy() {
		super.destroy();
		try {
			_scheduler.cancel();
			doDeregister();
		}
		catch ( Exception e ) {
			LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );

		}
		finally {
			try {
				disableObservers();
				close();
			}
			catch ( IOException e ) {
				LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
				_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHomePath() {
		return _homePath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHomePath( String aHomePath ) {
		_homePath = aHomePath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStatusPath() {
		return _statusPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStatusPath( String aStatusPath ) {
		_statusPath = aStatusPath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _server.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setObserversActive( boolean isActive ) {
		_server.setObserversActive( isActive );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isObserversActive() {
		return _server.isObserversActive();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		_server.setHttpExceptionHandler( aHttpErrorHandler );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpExceptionHandler getHttpExceptionHandler() {
		return _server.getHttpExceptionHandler();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		_server.setHttpExceptionHandling( aHttpErrorHandling );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpExceptionHandling getHttpExceptionHandling() {
		return _server.getHttpExceptionHandling();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaDataCenterType getEurekaDataCenterType() {
		return _dataCenterType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEurekaDataCenterType( EurekaDataCenterType aDataCenterType ) {
		_dataCenterType = aDataCenterType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHomeRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		_homeRequestConsumer = aRequestConsumer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getHomeRequestConsumer() {
		return _homeRequestConsumer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStatusRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		_statusRequestConsumer = aRequestConsumer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getStatusRequestConsumer() {
		return _statusRequestConsumer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withRealm( String aRealm ) {
		_server.setRealm( aRealm );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withCloseUnchecked() {
		closeUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withObserversActive( boolean isActive ) {
		_server.setObserversActive( isActive );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withEnableObservers() {
		enableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withDisableObservers() {
		disableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withOnHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
		onHttpException( aHttpExceptionHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withOpenUnchecked( HttpServerContext aConnection ) {
		openUnchecked( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		_server.setHttpExceptionHandler( aHttpErrorHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		_server.setHttpExceptionHandling( aHttpErrorHandling );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withBaseLocator( String aBaseLocator ) {
		_server.setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withOpen( HttpServerContext aConnection ) throws IOException {
		open( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withOpen( int aPort ) throws IOException {
		open( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withPort( int aPort ) {
		_server.setPort( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withScheme( Scheme aScheme ) {
		_server.setScheme( aScheme );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withProtocol( String aProtocol ) {
		_server.setProtocol( aProtocol );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		_server.setKeyStoreDescriptor( aKeyStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withMaxConnections( int aMaxConnections ) {
		_server.setMaxConnections( aMaxConnections );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withEurekaDataCenterType( EurekaDataCenterType aDataCenterType ) {
		setEurekaDataCenterType( aDataCenterType );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHomePath( String aHomePath ) {
		setHomePath( aHomePath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withStatusPath( String aStatusPath ) {
		setStatusPath( aStatusPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withAlias( String aAlias ) {
		setAlias( aAlias );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withPingPath( String aPingPath ) {
		setPingPath( aPingPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHttpRegistryUrl( Url aUrl ) {
		setHttpRegistryUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHttpServerDescriptor( EurekaServerDescriptor aServerDescriptor ) {
		setHttpServerDescriptor( aServerDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHost( String aHost ) {
		setHost( aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withVirtualHost( String aVirtualHost ) {
		setVirtualHost( aVirtualHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withIpAddress( int[] aIpAddress ) {
		setIpAddress( aIpAddress );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withCidrNotation( String aCidrNotation ) {
		fromCidrNotation( aCidrNotation );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withInstanceId( String aInstanceId ) {
		setInstanceId( aInstanceId );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withStatusRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setStatusRequestConsumer( aRequestConsumer );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServerDecorator withHomeRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setHomeRequestConsumer( aRequestConsumer );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// TWEAKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaServerDescriptor toHttpServerDescriptor( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, String aStatusPath, String aHomePath, EurekaDataCenterType aDataCenterType ) {
		return EurekaRegistrySidecar.toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, aStatusPath, aHomePath, aDataCenterType, this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Registers the given service at Eureka.
	 * 
	 * @param aServiceStatus The {@link EurekaServiceStatus} to be set.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.t
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	protected void doRegister( EurekaServiceStatus aServiceStatus ) throws HttpStatusException, IOException {
		EurekaRegistrySidecar.doRegister( aServiceStatus, this, _executorService );
	}

	/**
	 * Does a Eureka status update for the given service.
	 * 
	 * @param aServiceStatus The {@link EurekaServiceStatus} to be set.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.t
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 * @throws UnknownHostException Thrown in case the targeted host is unknown.
	 */
	protected void doStatusUpdate( EurekaServiceStatus aServiceStatus ) throws IOException, HttpStatusException {
		EurekaRegistrySidecar.doStatusUpdate( aServiceStatus, this, _executorService );
	}

	/**
	 * Unregisters the given service at Eureka.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.t
	 * @throws IOException Thrown in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	protected void doDeregister() throws IOException, HttpStatusException {
		EurekaRegistrySidecar.doDeregister( this, _executorService );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Resolves the property from the provided value and this instance and sets
	 * the property in case the provided value is not null.
	 * 
	 * @param aRegistryUrl The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	@Override
	protected Url toHttpRegistryUrl( Url aRegistryUrl ) {
		aRegistryUrl = super.toHttpRegistryUrl( aRegistryUrl );
		if ( aRegistryUrl != null && aRegistryUrl.getPath() == null ) {
			aRegistryUrl = new Url( aRegistryUrl, EUREKA_BASE_PATH );
			setHttpRegistryUrl( aRegistryUrl );
		}
		return aRegistryUrl;
	}

	/**
	 * Resolves the property from the provided value and this instance and sets
	 * the property in case the provided value is not null.
	 * 
	 * @param aHomeRequestObserver The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected RestRequestConsumer toHomeObserver( RestRequestConsumer aHomeRequestObserver ) {
		if ( aHomeRequestObserver == null ) {
			aHomeRequestObserver = _homeRequestConsumer;
			if ( aHomeRequestObserver == null ) {
				aHomeRequestObserver = ( aReq, aResp ) -> { aResp.getHeaderFields().putContentType( MediaType.TEXT_PLAIN ); aResp.setResponse( "Pong!" ); LOGGER.info( "Received a HOME request, no HOME handler defined, using default handler!" ); };
				_homeRequestConsumer = aHomeRequestObserver;
			}
		}
		return aHomeRequestObserver;
	}

	/**
	 * Resolves the property from the provided value and this instance and sets
	 * the property in case the provided value is not null.
	 * 
	 * @param aStatusRequestObserver The value to be used when not null.
	 * 
	 * @return The value when not null, else the value of the provided property.
	 */
	protected RestRequestConsumer toStatusObserver( RestRequestConsumer aStatusRequestObserver ) {
		if ( aStatusRequestObserver == null ) {
			aStatusRequestObserver = _statusRequestConsumer;
			if ( aStatusRequestObserver == null ) {
				aStatusRequestObserver = ( aReq, aResp ) -> { aResp.getHeaderFields().putContentType( MediaType.TEXT_PLAIN ); aResp.setResponse( "Pong!" ); LOGGER.info( "Received a STATUS request, no STATUS handler defined, using default handler!" ); };
				_statusRequestConsumer = aStatusRequestObserver;
			}
		}
		return aStatusRequestObserver;
	}
}
