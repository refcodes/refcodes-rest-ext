// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleComponent;
import org.refcodes.data.Scheme;
import org.refcodes.rest.HomeRequestObserver;
import org.refcodes.rest.HomeRequestObserverAccessor;
import org.refcodes.rest.HomeRequestObserverAccessor.HomeRequestObserverBuilder;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.RestfulHttpRegistryServer;
import org.refcodes.rest.StatusRequestObserver;
import org.refcodes.rest.StatusRequestObserverAccessor;
import org.refcodes.rest.StatusRequestObserverAccessor.StatusRequestObserverBuilder;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * The {@link RestfulEurekaServer} refines the {@link RestfulHttpRegistryServer}
 * for use with an Eureka Service-Registry. The Lifecycle-States are used to
 * control the registry process.
 * <ol>
 * <li>{@link #initialize()}: (or the like <code>initialize</code> methods)
 * Initialize this service by registering it with the status
 * {@link EurekaServiceStatus#STARTING} to the attached registry server.
 * <li>{@link #start()}: Starts this service by registering it with the status
 * {@link EurekaServiceStatus#UP} to the attached registry server.
 * <li>{@link #pause()}: Pauses this service by updating the status
 * {@link EurekaServiceStatus#DOWN} to the attached registry server.
 * <li>{@link #resume()}:
 * <li>{@link #stop()}: Stops this service by updating the status
 * {@link EurekaServiceStatus#OUT_OF_SERVICE} to the attached registry server.
 * <li>{@link #destroy()}: Removes the server from the registry.
 * </ol>
 * The {@link RestfulEurekaServer} implements the {@link LifecycleComponent},
 * the above mentioned lifecycle states behave as of the definition for the
 * {@link LifecycleComponent} interface.
 */
public interface RestfulEurekaServer extends RestfulHttpRegistryServer<EurekaServerDescriptor, RestfulEurekaServer>, EurekaRegistry<RestfulEurekaServer>, HomeRequestObserver, StatusRequestObserver, StatusRequestObserverAccessor, StatusRequestObserverBuilder<RestfulEurekaServer>, HomeRequestObserverAccessor, HomeRequestObserverBuilder<RestfulEurekaServer>, EurekaServerDescriptorFactory {

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the Eureka service registry with a status such as "starting" or
	 * "initializing" or "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aRegistryContext The context providing the descriptor of the
	 *        server to be registered and the {@link Url} of the service
	 *        registry to be used as well as the required truststore.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( EurekaRegistryContext aRegistryContext ) throws InitializeException {
		initialize( aRegistryContext.getHttpServerDescriptor(), aRegistryContext.getPingRequestObserver(), aRegistryContext.getStatusRequestConsumer(), aRegistryContext.getHomeRequestConsumer(), aRegistryContext.getHttpRegistryUrl(), aRegistryContext.getTrustStoreDescriptor() );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry with a status such as "starting" or "initializing"
	 * or "not-ready-yet". {@inheritDoc}
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered.
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aPingRequestObserver The {@link RestRequestConsumer} hooking into
	 *        a ping request.
	 * @param aRegistryUrl The registry server where to register.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	@Override
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, RestRequestConsumer aPingRequestObserver, Url aRegistryUrl ) throws InitializeException {
		initialize( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, aPingRequestObserver, null, null, null, null, null, aRegistryUrl, null );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry.
	 * 
	 * @param aAlias The name ("alias") which identifies the server in the
	 *        registry.
	 * @param aInstanceId The TID for the instance when being registered at the
	 *        service registry. If omitted, then the host name is used.
	 * @param aScheme The {@link Scheme} to which this server is being attached
	 *        (HTTP or HTTPS).
	 * @param aHost The host name to be used to address this server. If omitted,
	 *        then the system's host name should be used.
	 * @param aVirtualHost The virtual host name to be used for resolving.
	 * @param aIpAddress The IP-Address identifying the host.
	 * @param aPort The port of your service being registered. Make sure, you do
	 *        not
	 * @param aPingPath The path to use as health-check end-point by this
	 *        server.
	 * @param aPingRequestObserver The health-check request observer hook.
	 * @param aStatusPath The path to use as status-page end-point by this
	 * @param aStatusRequestObserver The status-page request observer hook.
	 * @param aHomePath The path to use as home-page end-point by this
	 * @param aHomeRequestObserver The home-page request observer hook.
	 * @param aDataCenterType The type of the data center your
	 *        {@link RestfulEurekaServer} is running in.
	 * @param aRegistryUrl The registry server where to register.
	 * @param aStoreDescriptor The descriptor describing the truststore for
	 *        (optionally) opening an HTTPS connection to the registry server.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	default void initialize( String aAlias, String aInstanceId, Scheme aScheme, String aHost, String aVirtualHost, int[] aIpAddress, int aPort, String aPingPath, RestRequestConsumer aPingRequestObserver, String aStatusPath, RestRequestConsumer aStatusRequestObserver, String aHomePath, RestRequestConsumer aHomeRequestObserver, EurekaDataCenterType aDataCenterType, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		initialize( toHttpServerDescriptor( aAlias, aInstanceId, aScheme, aHost, aVirtualHost, aIpAddress, aPort, aPingPath, aStatusPath, aHomePath, aDataCenterType ), aPingRequestObserver, aStatusRequestObserver, aHomeRequestObserver, aRegistryUrl, aStoreDescriptor );
	}

	/**
	 * Initializes the {@link RestfulHttpRegistryServer} by registering it at
	 * the service registry.
	 * 
	 * @param aServerDescriptor The descriptor describing the server.
	 * @param aPingRequestObserver The health-check request observer hook.
	 * @param aStatusRequestObserver The status-page request observer hook.
	 * @param aHomeRequestObserver The home-page request observer hook.
	 * @param aRegistryUrl The registry server where to register.
	 * @param aStoreDescriptor The descriptor describing the truststore for
	 *        (optionally) opening an HTTPS connection to the registry server.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	void initialize( EurekaServerDescriptor aServerDescriptor, RestRequestConsumer aPingRequestObserver, RestRequestConsumer aStatusRequestObserver, RestRequestConsumer aHomeRequestObserver, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void initialize( EurekaServerDescriptor aServerDescriptor, Url aRegistryUrl, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		initialize( aServerDescriptor, null, null, null, aRegistryUrl, aStoreDescriptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulEurekaServer withStatusRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setStatusRequestConsumer( aRequestConsumer );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default RestfulEurekaServer withHomeRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setHomeRequestConsumer( aRequestConsumer );
		return this;
	}
}
