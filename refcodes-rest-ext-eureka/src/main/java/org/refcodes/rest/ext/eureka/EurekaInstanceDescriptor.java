package org.refcodes.rest.ext.eureka;

import java.net.MalformedURLException;
import java.util.Map;

import org.refcodes.data.Delimiter;
import org.refcodes.exception.BugException;
import org.refcodes.mixin.AliasAccessor.AliasProperty;
import org.refcodes.mixin.PortAccessor.PortProperty;
import org.refcodes.net.IpAddress;
import org.refcodes.net.IpAddressAccessor.IpAddressProperty;
import org.refcodes.rest.HomeUrlAccessor.HomeUrlProperty;
import org.refcodes.rest.PingUrlAccessor.PingUrlProperty;
import org.refcodes.rest.StatusUrlAccessor.StatusUrlProperty;
import org.refcodes.web.HostAccessor.HostProperty;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.Url;

public class EurekaInstanceDescriptor extends HttpBodyMap implements AliasProperty, PingUrlProperty, StatusUrlProperty, HomeUrlProperty, HostProperty, IpAddressProperty, PortProperty {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link EurekaInstanceDescriptor} instance using the
	 * public path delimiter "/" ({@link Delimiter#PATH}) for the path
	 * declarations.
	 */
	public EurekaInstanceDescriptor() {}

	/**
	 * Creates a {@link EurekaInstanceDescriptor} instance containing the
	 * elements as of {@link MutablePathMap#insert(Object)} using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public EurekaInstanceDescriptor( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Create a {@link EurekaInstanceDescriptor} instance containing the
	 * elements as of {@link MutablePathMap#insert(Object)} using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public EurekaInstanceDescriptor( Object aObj ) {
		super( aObj );
	}

	/**
	 * Creates a {@link EurekaInstanceDescriptor} instance containing the
	 * elements as of {@link MutablePathMap#insert(Object)} using the default
	 * path delimiter "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public EurekaInstanceDescriptor( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	/**
	 * Create a {@link EurekaInstanceDescriptor} instance containing the
	 * elements as of {@link MutablePathMap#insert(Object)} using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public EurekaInstanceDescriptor( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public String getActionType() {
		return get( "/actionType" );
	}

	public void setActionType( String aValue ) {
		put( "/actionType", aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return get( "/app" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAlias( String aAlias ) {
		put( "/app", aAlias );
	}

	public String getCountryId() {
		return get( "/countryId" );
	}

	public void setCountryId( String aValue ) {
		put( "/countryId", aValue );
	}

	public String getDataCenterInfoClass() {
		return get( "/dataCenterInfo/@class" );
	}

	public void setDataCenterInfoClass( String aValue ) {
		put( "/dataCenterInfo/@class", aValue );
	}

	public String getDataCenterInfoName() {
		return get( "/dataCenterInfo/name" );
	}

	public void setDataCenterInfoName( String aValue ) {
		put( "/dataCenterInfo/name", aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getPingUrl() {
		final String theHealthCheckUrl = get( "/healthCheckUrl" );
		if ( theHealthCheckUrl == null ) {
			return null;
		}
		try {
			return new Url( theHealthCheckUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theHealthCheckUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPingUrl( Url aUrl ) {
		put( "/healthCheckUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getStatusUrl() {
		final String theStatusPageUrl = get( "/statusPageUrl" );
		if ( theStatusPageUrl == null ) {
			return null;
		}
		try {
			return new Url( theStatusPageUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theStatusPageUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStatusUrl( Url aUrl ) {
		put( "/statusPageUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHomeUrl() {
		final String theHomePageUrl = get( "/homePageUrl" );
		if ( theHomePageUrl == null ) {
			return null;
		}
		try {
			return new Url( theHomePageUrl );
		}
		catch ( MalformedURLException e ) {
			throw new BugException( "Cannot parse malformed Health-Check URL <" + theHomePageUrl + ">!", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHomeUrl( Url aUrl ) {
		put( "/homePageUrl", aUrl.toHttpUrl() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHost() {
		return get( "/hostName" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHost( String aHost ) {
		put( "/hostName", aHost );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int[] getIpAddress() {
		return IpAddress.fromAnyCidrNotation( get( "/ipAddr" ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setIpAddress( int[] aIpAddress ) {
		put( "/ipAddr", IpAddress.toString( aIpAddress ) );
	}

	public String isCoordinatingDiscoveryServer() {
		return get( "/isCoordinatingDiscoveryServer" );
	}

	public void setCoordinatingDiscoveryServer( String aValue ) {
		put( "/isCoordinatingDiscoveryServer", aValue );
	}

	public Long getLastDirtyTimestamp() {
		return getLong( "/lastDirtyTimestamp" );
	}

	public void setLastDirtyTimestamp( Long aValue ) {
		putLong( "/lastDirtyTimestamp", aValue );
	}

	public Long getLastUpdatedTimestamp() {
		return getLong( "/lastUpdatedTimestamp" );
	}

	public void setLastUpdatedTimestamp( Long aValue ) {
		putLong( "/lastUpdatedTimestamp", aValue );
	}

	public Integer getLeaseInfoDurationInSecs() {
		return getInt( "/leaseInfo/durationInSecs" );
	}

	public void setLeaseInfoDurationInSecs( Integer aValue ) {
		putInt( "/leaseInfo/durationInSecs", aValue );
	}

	public Long getLeaseInfoEvictionTimestamp() {
		return getLong( "/leaseInfo/evictionTimestamp" );
	}

	public void setLeaseInfoEvictionTimestamp( Long aValue ) {
		putLong( "/leaseInfo/evictionTimestamp", aValue );
	}

	public Long getLeaseInfoLastRenewalTimestamp() {
		return getLong( "/leaseInfo/lastRenewalTimestamp" );
	}

	public void setLeaseInfoLastRenewalTimestamp( Long aValue ) {
		putLong( "/leaseInfo/lastRenewalTimestamp", aValue );
	}

	public Long getLeaseInfoRegistrationTimestamp() {
		return getLong( "/leaseInfo/registrationTimestamp" );
	}

	public void setLeaseInfoRegistrationTimestamp( Long aValue ) {
		putLong( "/leaseInfo/registrationTimestamp", aValue );
	}

	public Integer getLeaseInfoRenewalIntervalInSecs() {
		return getInt( "/leaseInfo/renewalIntervalInSecs" );
	}

	public void setLeaseInfoRenewalIntervalInSecs( Integer aValue ) {
		putInt( "/leaseInfo/renewalIntervalInSecs", aValue );
	}

	public Long getLeaseInfoServiceUpTimestamp() {
		return getLong( "/leaseInfo/serviceUpTimestamp" );
	}

	public void setLeaseInfoServiceUpTimestamp( Long aValue ) {
		putLong( "/leaseInfo/serviceUpTimestamp", aValue );
	}

	public String getMetadataClass() {
		return get( "/metadata/@class" );
	}

	public void setMetadataClass( String aValue ) {
		put( "/metadata/@class", aValue );
	}

	public String getOverriddenstatus() {
		return get( "/overriddenstatus" );
	}

	public void setOverriddenstatus( String aValue ) {
		put( "/overriddenstatus", aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPort() {
		final Integer thePort = getInt( "/port/$" );
		return thePort != null ? thePort : -1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPort( int aPort ) {
		putInt( "/port/$", aPort );
	}

	public String getPortEnabled() {
		return get( "/port/@enabled" );
	}

	public void setPortEnabled( String aValue ) {
		put( "/port/@enabled", aValue );
	}

	public Integer getSecurePort() {
		return getInt( "/securePort/$" );
	}

	public void setSecurePort( Integer aValue ) {
		putInt( "/securePort/$", aValue );
	}

	public String getSecurePortEnabled() {
		return get( "/securePort/@enabled" );
	}

	public void setSecurePortEnabled( String aValue ) {
		put( "/securePort/@enabled", aValue );
	}

	public String getStatus() {
		return get( "/status" );
	}

	public void setStatus( String aValue ) {
		put( "/status", aValue );
	}

	public String getVipAddress() {
		return get( "/vipAddress" );
	}

	public void setVipAddress( String aValue ) {
		put( "/vipAddress", aValue );
	}

	public Map<String, String> getMetaData() {
		return retrieveFrom( "/metadata" );
	}

	public void setMetaData( Map<String, String> aMetaData ) {
		for ( String eKey : aMetaData.keySet() ) {
			put( toPath( "/metadata", eKey ), aMetaData.get( eKey ) );
		}
	}

	public String putMetaData( String aKey, String aValue ) {
		return put( toPath( "/metadata", aKey ), aValue );
	}
}