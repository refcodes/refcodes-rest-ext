// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import org.refcodes.data.Scheme;
import org.refcodes.rest.HttpExceptionHandler;
import org.refcodes.rest.HttpExceptionHandling;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.security.KeyStoreDescriptor;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.HttpServerContext;
import org.refcodes.web.Url;

/**
 * The {@link EurekaRestServer} enriches the {@link RestfulHttpServer} with
 * functionality such registering and unregistering from / to a Eureka discovery
 * service. Follow the documentation of the {@link RestfulEurekaServer} in order
 * to initiate the states such as {@link EurekaServiceStatus#UP},
 * {@link EurekaServiceStatus#DOWN} or
 * {@link EurekaServiceStatus#OUT_OF_SERVICE} and unregistering.
 */
public class EurekaRestServer extends EurekaRestServerDecorator {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link EurekaRestServer} with discovery functionality. Use
	 * {@link #initialize()}, {@link #start()}, {@link #pause()},
	 * {@link #resume()}, {@link #stop()} and {@link #destroy()} for publishing
	 * status updates to Eureka. Use
	 * {@link #open(org.refcodes.web.HttpServerContext)} or similar to activate
	 * your server.
	 */
	public EurekaRestServer() {
		super( new HttpRestServer() );
	}

	/**
	 * Constructs a {@link EurekaRestServer} with discovery functionality. Use
	 * {@link #initialize()}, {@link #start()}, {@link #pause()},
	 * {@link #resume()}, {@link #stop()} and {@link #destroy()} for publishing
	 * status updates to Eureka. Use
	 * {@link #open(org.refcodes.web.HttpServerContext)} or similar to activate
	 * your server.
	 *
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public EurekaRestServer( ExecutorService aExecutorService ) {
		super( new HttpRestServer( aExecutorService ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withRealm( String aRealm ) {
		setRealm( aRealm );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withCloseUnchecked() {
		closeUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withObserversActive( boolean isActive ) {
		setObserversActive( isActive );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withEnableObservers() {
		enableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withDisableObservers() {
		disableObservers();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withOnHttpException( HttpExceptionHandler aHttpExceptionHandler ) {
		onHttpException( aHttpExceptionHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withOpenUnchecked( HttpServerContext aConnection ) {
		openUnchecked( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHttpExceptionHandler( HttpExceptionHandler aHttpErrorHandler ) {
		setHttpExceptionHandler( aHttpErrorHandler );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHttpExceptionHandling( HttpExceptionHandling aHttpErrorHandling ) {
		setHttpExceptionHandling( aHttpErrorHandling );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withBaseLocator( String aBaseLocator ) {
		setBaseLocator( aBaseLocator );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withClose() throws IOException {
		close();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withCloseQuietly() {
		closeQuietly();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withCloseIn( int aCloseMillis ) {
		closeIn( aCloseMillis );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withOpen( HttpServerContext aConnection ) throws IOException {
		open( aConnection );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withOpen( int aPort ) throws IOException {
		open( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withPort( int aPort ) {
		setPort( aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withScheme( Scheme aScheme ) {
		setScheme( aScheme );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withProtocol( String aProtocol ) {
		setProtocol( aProtocol );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withKeyStoreDescriptor( KeyStoreDescriptor aKeyStoreDescriptor ) {
		setKeyStoreDescriptor( aKeyStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withMaxConnections( int aMaxConnections ) {
		setMaxConnections( aMaxConnections );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withEurekaDataCenterType( EurekaDataCenterType aDataCenterType ) {
		setEurekaDataCenterType( aDataCenterType );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHomePath( String aHomePath ) {
		setHomePath( aHomePath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withStatusPath( String aStatusPath ) {
		setStatusPath( aStatusPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withAlias( String aAlias ) {
		setAlias( aAlias );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withPingPath( String aPingPath ) {
		setPingPath( aPingPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHttpRegistryUrl( Url aUrl ) {
		setHttpRegistryUrl( aUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHttpServerDescriptor( EurekaServerDescriptor aServerDescriptor ) {
		setHttpServerDescriptor( aServerDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHost( String aHost ) {
		setHost( aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withVirtualHost( String aVirtualHost ) {
		setVirtualHost( aVirtualHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withIpAddress( int[] aIpAddress ) {
		setIpAddress( aIpAddress );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withCidrNotation( String aCidrNotation ) {
		fromCidrNotation( aCidrNotation );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withInstanceId( String aInstanceId ) {
		setInstanceId( aInstanceId );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withStatusRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setStatusRequestConsumer( aRequestConsumer );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestServer withHomeRequestConsumer( RestRequestConsumer aRequestConsumer ) {
		setHomeRequestConsumer( aRequestConsumer );
		return this;
	}
}
