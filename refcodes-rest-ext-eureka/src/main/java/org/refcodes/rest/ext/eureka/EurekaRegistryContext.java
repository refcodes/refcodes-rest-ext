// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.rest.HomeRequestObserverAccessor.HomeRequestObserverBuilder;
import org.refcodes.rest.HomeRequestObserverAccessor.HomeRequestObserverProperty;
import org.refcodes.rest.HttpRegistryContext;
import org.refcodes.rest.HttpRegistryContext.HttpRegistryContextBuilder;
import org.refcodes.rest.RestRequestConsumer;
import org.refcodes.rest.StatusRequestObserverAccessor.StatusRequestObserverBuilder;
import org.refcodes.rest.StatusRequestObserverAccessor.StatusRequestObserverProperty;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.Url;

/**
 * Implementation of the {@link EurekaRegistryContext} representing a
 * waschechten {@link EurekaRegistryContext} being a
 * https://www.metacodes.proization of the {@link HttpRegistryContext}.
 */
public class EurekaRegistryContext implements HttpRegistryContext<EurekaServerDescriptor>, HttpRegistryContextBuilder<EurekaServerDescriptor>, StatusRequestObserverProperty, StatusRequestObserverBuilder<EurekaRegistryContext>, HomeRequestObserverProperty, HomeRequestObserverBuilder<EurekaRegistryContext> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Url _registryUrl;
	private EurekaServerDescriptor _serverDescriptor;
	private RestRequestConsumer _homeRequestObserver;
	private RestRequestConsumer _pingRequestObserver;
	private RestRequestConsumer _statusRequestObserver;
	private TrustStoreDescriptor _storeDescriptor;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link EurekaRegistryContext} instance.
	 */
	public EurekaRegistryContext() {}

	/**
	 * Constructs an {@link EurekaRegistryContext} instance.
	 * 
	 * @param aServerDescriptor The descriptor describing the server.
	 * @param aRegistryUrl The {@link Url} pointing to the registry service.
	 * @param aPingRequestObserver The ping (health) request observer.
	 * @param aStatusRequestObserver The status(-page) observer.
	 * @param aHomeRequestObserver The home(-page) observer.
	 * @param aStoreDescriptor The trust store descriptor describing the trust
	 *        store to be used when requesting the registry service.
	 */
	public EurekaRegistryContext( EurekaServerDescriptor aServerDescriptor, Url aRegistryUrl, RestRequestConsumer aPingRequestObserver, RestRequestConsumer aStatusRequestObserver, RestRequestConsumer aHomeRequestObserver, TrustStoreDescriptor aStoreDescriptor ) {
		_serverDescriptor = aServerDescriptor;
		_registryUrl = aRegistryUrl;
		_homeRequestObserver = aHomeRequestObserver;
		_pingRequestObserver = aPingRequestObserver;
		_statusRequestObserver = aStatusRequestObserver;
		_storeDescriptor = aStoreDescriptor;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getHomeRequestConsumer() {
		return _homeRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url getHttpRegistryUrl() {
		return _registryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaServerDescriptor getHttpServerDescriptor() {
		return _serverDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getPingRequestObserver() {
		return _pingRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestRequestConsumer getStatusRequestConsumer() {
		return _statusRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TrustStoreDescriptor getTrustStoreDescriptor() {
		return _storeDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHomeRequestObserver( RestRequestConsumer aHomeRequestObserver ) {
		_homeRequestObserver = aHomeRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpRegistryUrl( Url aRegistryUrl ) {
		_registryUrl = aRegistryUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHttpServerDescriptor( EurekaServerDescriptor aServerDescriptor ) {
		_serverDescriptor = aServerDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPingRequestObserver( RestRequestConsumer aPingRequestObserver ) {
		_pingRequestObserver = aPingRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStatusRequestObserver( RestRequestConsumer aStatusRequestObserver ) {
		_statusRequestObserver = aStatusRequestObserver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrustStoreDescriptor( TrustStoreDescriptor aTrustStoreDescriptor ) {
		_storeDescriptor = aTrustStoreDescriptor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRegistryContext withHomeRequestConsumer( RestRequestConsumer aRequestObserver ) {
		setHomeRequestObserver( aRequestObserver );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRegistryContext withStatusRequestConsumer( RestRequestConsumer aRequestObserver ) {
		setStatusRequestObserver( aRequestObserver );
		return this;
	}
}
