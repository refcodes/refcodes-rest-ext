// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;

import org.refcodes.data.Scheme;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.HttpClientContext;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.OauthToken;
import org.refcodes.web.Url;

/**
 * The {@link EurekaRestClient} enriches the {@link RestfulHttpClient} with
 * functionality such registering and unregistering from / to a Eureka discovery
 * service.
 */
public class EurekaRestClient extends EurekaRestClientDecorator {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link EurekaRestClient} with discovery functionality. Use
	 * {@link #open()} or similar to activate it.
	 */
	public EurekaRestClient() {
		super( new HttpRestClient() );
	}

	/**
	 * Constructs a {@link EurekaRestClient} with discovery functionality. Use
	 * {@link #open()} or similar to activate it.
	 *
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public EurekaRestClient( ExecutorService aExecutorService ) {
		super( new HttpRestClient( aExecutorService ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpen() throws IOException {
		open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpen( HttpClientContext aCtx ) throws IOException {
		open( aCtx );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpen( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpen( Url aBaseUrl ) throws IOException {
		open( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpen( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		open( aBaseUrl, aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( Url aBaseUrl ) {
		setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( URL aBaseURL ) {
		setBaseUrl( aBaseURL );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		setBasicAuthCredentials( aBasicAuthCredentials );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBasicAuthCredentials( String aUserName, String aSecret ) {
		setBasicAuthCredentials( aUserName, aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOAuthToken( OauthToken aOauthToken ) {
		setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withUserAgent( String aUserAgent ) {
		setUserAgent( aUserAgent );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aProtocol, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
		setBaseUrl( aScheme, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withOpenUnchecked() {
		openUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		setLoadBalancingStrategy( aStrategy );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClient withHttpDiscoveryUrl( Url aUrl ) {
		setHttpDiscoveryUrl( aUrl );
		return this;
	}
}
