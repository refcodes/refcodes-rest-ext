// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

/**
 * Provides an accessor for a Amazon Meta-Data property.
 */
public interface AmazonMetaDataAccessor {

	/**
	 * Retrieves the Amazon Meta-Data from the Amazon Meta-Data property.
	 * 
	 * @return The Amazon Meta-Data stored by the Amazon Meta-Data property.
	 */
	AmazonMetaData getAmazonMetaData();

	/**
	 * Provides a mutator for a Amazon Meta-Data property.
	 */
	public interface AmazonMetaDataMutator {

		/**
		 * Sets the Amazon Meta-Data for the Amazon Meta-Data property.
		 * 
		 * @param aDataCenterType The Amazon Meta-Data to be stored by the
		 *        Amazon Meta-Data property.
		 */
		void setAmazonMetaData( AmazonMetaData aDataCenterType );
	}

	/**
	 * Provides a mutator for an Amazon Meta-Data property.
	 * 
	 * @param <B> The builder which implements the
	 *        {@link AmazonMetaDataBuilder}.
	 */
	public interface AmazonMetaDataBuilder<B extends AmazonMetaDataBuilder<?>> {

		/**
		 * Sets the Amazon Meta-Data to use and returns this builder as of the
		 * builder pattern.
		 * 
		 * @param aDataCenterType The Amazon Meta-Data to be stored by the
		 *        Amazon Meta-Data property.
		 * 
		 * @return This {@link AmazonMetaDataBuilder} instance to continue
		 *         configuration.
		 */
		B withAmazonMetaData( AmazonMetaData aDataCenterType );
	}

	/**
	 * Provides a Amazon Meta-Data property.
	 */
	public interface AmazonMetaDataProperty extends AmazonMetaDataAccessor, AmazonMetaDataMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link AmazonMetaData}
		 * (setter) as of {@link #setAmazonMetaData(AmazonMetaData)} and returns
		 * the very same value (getter).
		 * 
		 * @param aAmazonMetaData The {@link AmazonMetaData} to set (via
		 *        {@link #setAmazonMetaData(AmazonMetaData)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default AmazonMetaData letAmazonMetaData( AmazonMetaData aAmazonMetaData ) {
			setAmazonMetaData( aAmazonMetaData );
			return aAmazonMetaData;
		}
	}
}
