// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.data.Scheme;
import org.refcodes.exception.Trap;
import org.refcodes.rest.AbstractRestfulHttpDiscoveryClientDecorator;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.rest.ext.eureka.EurekaDiscoverySidecar.RefreshDaemon;
import org.refcodes.security.TrustStoreDescriptor;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.HttpClientContext;
import org.refcodes.web.LoadBalancingStrategy;
import org.refcodes.web.OauthToken;
import org.refcodes.web.Url;

/**
 * The {@link EurekaRestClientDecorator} decorates a {@link RestfulHttpClient}
 * with functionality such registering and unregistering from / to a Eureka
 * discovery service.
 */
public class EurekaRestClientDecorator extends AbstractRestfulHttpDiscoveryClientDecorator<RestfulEurekaClient> implements RestfulEurekaClient {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( EurekaRestClientDecorator.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExecutorService _executorService;
	private RefreshDaemon _refreshDaemon;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link RestfulHttpClient} with discovery
	 * functionality.
	 * 
	 * @param aClient The {@link RestfulHttpClient} to be decorated.
	 */
	public EurekaRestClientDecorator( RestfulHttpClient aClient ) {
		super( aClient );
	}

	/**
	 * Decorates the given {@link RestfulHttpClient} with discovery
	 * functionality.
	 * 
	 * @param aClient The {@link RestfulHttpClient} to be decorated.
	 * @param aExecutorService An executor service to be used when creating
	 *        {@link Thread}s.
	 */
	public EurekaRestClientDecorator( RestfulHttpClient aClient, ExecutorService aExecutorService ) {
		super( aClient );
		_executorService = aExecutorService;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFE-CYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize( Url aDiscoveryUrl, LoadBalancingStrategy aStrategy, TrustStoreDescriptor aStoreDescriptor ) throws InitializeException {
		aDiscoveryUrl = toHttpDiscoveryUrl( aDiscoveryUrl );
		aStrategy = toLoadBalancerStrategy( aStrategy );
		super.initialize();
		setLoadBalancingStrategy( aStrategy );
		try {
			_refreshDaemon = new RefreshDaemon( aDiscoveryUrl, aStoreDescriptor, this, _executorService );
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new InitializeException( "Cannot enter the initialize lifecycle!", e );
		}
	}

	@Override
	public synchronized void start() throws StartException {
		try {
			if ( !isOpened() ) {
				LOGGER.log( Level.WARNING, "This connection not opened yet (it is in status <" + getConnectionStatus() + ">, therefore will try to open this connection now..." );
				open();
			}
			super.start();
			_refreshDaemon.start();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StartException( "Cannot enter the start lifecycle!", e );
		}
	}

	@Override
	public synchronized void pause() throws PauseException {
		super.pause();
		try {}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new PauseException( "Cannot enter the pause lifecycle!", e );
		}
	}

	@Override
	public synchronized void stop() throws StopException {
		super.stop();
		try {
			_refreshDaemon.start();
		}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new StopException( "Cannot enter the stop lifecycle!", e );
		}
	}

	@Override
	public synchronized void resume() throws ResumeException {
		super.resume();
		try {}
		catch ( Exception e ) {
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			throw new ResumeException( "Cannot enter the resume lifecycle!", e );
		}
	}

	@Override
	public synchronized void destroy() {
		super.destroy();
		try {
			_refreshDaemon.destroy();
		}
		catch ( Exception e ) {
			LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
			_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );

		}
		finally {
			try {
				close();
			}
			catch ( IOException e ) {
				LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
				_lifeCycleAutomaton.setLifecycleStatus( LifecycleStatus.ERROR );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Url toUrl( Url aUrl ) {
		return EurekaDiscoverySidecar.toUrl( aUrl, this, _refreshDaemon );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BasicAuthCredentials getBasicAuthCredentials() {
		return _client.getBasicAuthCredentials();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		_client.setBasicAuthCredentials( aBasicAuthCredentials );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpen() throws IOException {
		_client.open();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpen( HttpClientContext aCtx ) throws IOException {
		_client.open( aCtx );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpen( TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		_client.open( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpen( Url aBaseUrl ) throws IOException {
		_client.open( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpen( Url aBaseUrl, TrustStoreDescriptor aStoreDescriptor ) throws IOException {
		_client.open( aBaseUrl, aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( String aBaseUrl ) throws MalformedURLException {
		_client.setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( Url aBaseUrl ) {
		_client.setBaseUrl( aBaseUrl );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( URL aBaseURL ) {
		_client.setBaseUrl( aBaseURL );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBasicAuthCredentials( BasicAuthCredentials aBasicAuthCredentials ) {
		_client.setBasicAuthCredentials( aBasicAuthCredentials );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBasicAuthCredentials( String aUserName, String aSecret ) {
		_client.setBasicAuthCredentials( aUserName, aSecret );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOAuthToken( OauthToken aOauthToken ) {
		_client.setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withTrustStoreDescriptor( TrustStoreDescriptor aStoreDescriptor ) {
		_client.setTrustStoreDescriptor( aStoreDescriptor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withUserAgent( String aUserAgent ) {
		_client.setUserAgent( aUserAgent );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( String aProtocol, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( Scheme aScheme, String aHost ) throws MalformedURLException {
		_client.setBaseUrl( aScheme, aHost );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( String aProtocol, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( Scheme aScheme, String aHost, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aScheme, aHost, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( String aProtocol, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( Scheme aScheme, String aHost, int aPort ) throws MalformedURLException {
		_client.setBaseUrl( aScheme, aHost, aPort );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( String aProtocol, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aProtocol, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withBaseUrl( Scheme aScheme, String aHost, int aPort, String aPath ) throws MalformedURLException {
		_client.setBaseUrl( aScheme, aHost, aPort, aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withOpenUnchecked() {
		openUnchecked();
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withLoadBalancingStrategy( LoadBalancingStrategy aStrategy ) {
		setLoadBalancingStrategy( aStrategy );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EurekaRestClientDecorator withHttpDiscoveryUrl( Url aUrl ) {
		setHttpDiscoveryUrl( aUrl );
		return this;
	}
}
