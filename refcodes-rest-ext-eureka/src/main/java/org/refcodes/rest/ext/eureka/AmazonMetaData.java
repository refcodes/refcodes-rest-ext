// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.rest.ext.eureka;

import org.refcodes.data.Delimiter;
import org.refcodes.web.HttpBodyMap;

/**
 * A {@link HttpBodyMap} with additional convenience attributes regarding
 * Amazon's meta data.
 */
public class AmazonMetaData extends HttpBodyMap {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link AmazonMetaData} instance using the public path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public AmazonMetaData() {}

	/**
	 * Create a {@link AmazonMetaData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public AmazonMetaData( Object aObj ) {
		super( aObj );
	}

	/**
	 * Create a {@link AmazonMetaData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public AmazonMetaData( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	/**
	 * Creates a {@link AmazonMetaData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public AmazonMetaData( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Creates a {@link AmazonMetaData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public AmazonMetaData( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public void setAmiLaunchIndex( Integer aValue ) {
		putInt( "/ami-launch-index", aValue );
	}

	public void setLocalHostName( String aValue ) {
		put( "/local-hostname", aValue );
	}

	public void setAvailabilityZone( String aValue ) {
		put( "/availability-zone", aValue );
	}

	public void setInstanceId( String aValue ) {
		put( "/instance-id", aValue );
	}

	public void setPublicIpv4( String aValue ) {
		put( "/public-ipv4", aValue );
	}

	public void setPublicHostname( String aValue ) {
		put( "/public-hostname", aValue );
	}

	public void setAmiManifestPath( String aValue ) {
		put( "/ami-manifest-path", aValue );
	}

	public void setLocalIpv4( String aValue ) {
		put( "/local-ipv4", aValue );
	}

	public void setHostname( String aValue ) {
		put( "/hostname", aValue );
	}

	public void setAmiId( String aValue ) {
		put( "/ami-id", aValue );
	}

	public void setInstanceType( String aValue ) {
		put( "/instance-type", aValue );
	}

	public Integer getAmiLaunchIndex() {
		return getInt( "/ami-launch-index" );
	}

	public String getLocalHostName() {
		return get( "/local-hostname" );
	}

	public String getAvailabilityZone() {
		return get( "/availability-zone" );
	}

	public String getInstanceId() {
		return get( "/instance-id" );
	}

	public String getPublicIpv4() {
		return get( "/public-ipv4" );
	}

	public String getPublicHostname() {
		return get( "/public-hostname" );
	}

	public String getAmiManifestPath() {
		return get( "/ami-manifest-path" );
	}

	public String getLocalIpv4() {
		return get( "/local-ipv4" );
	}

	public String getHostname() {
		return get( "/hostname" );
	}

	public String getAmiId() {
		return get( "/ami-id" );
	}

	public String getInstanceType() {
		return get( "/instance-type" );
	}
}
